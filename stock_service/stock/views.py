from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from django.db import Error, OperationalError
from django.db.transaction import atomic
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import Item
from .producer import publish_order_queue
from .serializers import ItemSerializer

from psycopg2 import errorcodes
from functools import wraps
import time


def retry_on_exception(view, num_retries=3, on_failure=Response(status=400), delay_=0.5, backoff_=1.5):
    @wraps(view)
    def retry(*args, **kwargs):
        delay = delay_
        for i in range(num_retries):
            try:
                return view(*args, **kwargs)
            except OperationalError as ex:
                if i == num_retries - 1:
                    return "Did not succeed with %d retries" % num_retries
                elif getattr(ex.__cause__, 'pgcode', '') == errorcodes.SERIALIZATION_FAILURE:
                    time.sleep(delay)
                    delay *= backoff_
                else:
                    return on_failure
            except Error as ex:
                return on_failure
    return retry


@method_decorator(csrf_exempt, name='dispatch')
class StockViewSet(viewsets.ViewSet):
    """
    GET - returns an item’s availability and price.
    Output JSON fields:
        “stock” - the item’s stock
        “price” - the item’s price
    """

    def find(self, request, item_id):
        item = Item.objects.get(id=int(item_id))
        serializer = ItemSerializer(item)
        return Response(serializer.data)

    """
    POST - subtracts an item from stock by the amount specified.
    """

    @retry_on_exception
    @atomic
    def subtract(self, request, item_id, number):
        item = get_object_or_404(Item, pk=int(item_id))
        item.stock -= int(number)
        item.stock = max(0, item.stock)
        item.save()

        return Response(status=200)

    """
    POST - adds the given number of stock items to the item count in the stock
    """

    @retry_on_exception
    @atomic
    def add(self, request, item_id, number):
        item = get_object_or_404(Item, pk=int(item_id))
        item.stock += number
        item.save()

        return Response(status=200)

    """
    POST - adds an item and its price, and returns its ID.
    Output JSON fields:
    “item_id” - the item’s id
    """

    @retry_on_exception
    @atomic
    def create(self, request, price):
        item = Item.objects.create(cost=float(price))
        serializer = ItemSerializer(instance=item)
        item.save()

        publish_order_queue('items_created', serializer.data)
        return JsonResponse({'item_id': item.id}, status=200)
