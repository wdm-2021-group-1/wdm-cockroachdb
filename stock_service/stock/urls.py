from django.urls import path, register_converter

from .converts import FloatUrlParameterConverter
from .views import StockViewSet

register_converter(FloatUrlParameterConverter, 'float')

urlpatterns = [
    path(
        'find/<int:item_id>/', StockViewSet.as_view(
            {
                'get': 'find'
            }
        )
    ),
    path(
        'subtract/<int:item_id>/<int:number>', StockViewSet.as_view(
            {
                'post': 'subtract',
            }
        )
    ),
    path(
        'add/<int:item_id>/<int:number>', StockViewSet.as_view(
            {
                'post': 'add',
            }
        )
    ),
    path(
        'item/create/<float:price>', StockViewSet.as_view(
            {
                'post': 'create',
            }
        )
    ),
]
