from django.db import models


class Item(models.Model):
    id = models.BigAutoField(primary_key=True)
    stock = models.IntegerField(default=0)
    cost = models.FloatField(default=0.0)
